import React, { useEffect, useState } from "react";

function ConferenceForm() {
    const [locations, setLocations] = useState([]);
    const [selectedLocation, setSelectedLocation] = useState('');
    const [name, setName] = useState('');
    const [starts, setStartDate] = useState('');
    const [ends, setEndDate] = useState('');
    const [description, setDescription] = useState('')
    const [max_presentations, setMaxPresentations] = useState('')
    const [max_attendees, setMaxAttendees] = useState('')

    const updateInputState = function ({ target }, cb) {
    const { value } = target;

    cb(value);
    };

    const handleSubmit = async function (event) {
    event.preventDefault();
    const data = {
        location: selectedLocation,
        name,
        starts,
        ends,
        description,
        max_presentations,
        max_attendees,
    };

    const conferenceUrl = "http://localhost:8000/api/conferences/";
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
        "Content-Type": "application/json",
        },
    };

    try {
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
        const newConference = await response.json();
        console.log("Conference created:", newConference);
        } else {
        throw new Error("Network response was not ok");
        }
    } catch (error) {
        console.error("Error creating conference:", error);
    }
    };

    const fetchData = async () => {
    const url = "http://localhost:8000/api/locations/";

    try {
        const response = await fetch(url);

        if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
        } else {
        throw new Error("Network response was not ok");
        }
    } catch (error) {
        console.error("Error fetching data:", error);
    }
    };

    useEffect(() => {
    fetchData();
    }, []);

    return (
    <>
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form id="create-conference-form" onSubmit={handleSubmit}>
                <div className="form-floating mb-3">
                <input
                    placeholder=""
                    required
                    type="text"
                    name="name"
                    id="name"
                    className="form-control"
                    onChange={(event) => updateInputState(event, setName)}
                />
                <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                <input
                    placeholder=""
                    required
                    type="date"
                    name="start_date"
                    id="start_date"
                    className="form-control"
                    onChange={(event) => updateInputState(event, setStartDate)}
                />
                <label htmlFor="start_date">Start Date</label>
                </div>
                <div className="form-floating mb-3">
                <input
                    placeholder=""
                    required
                    type="date"
                    name="end_date"
                    id="end_date"
                    className="form-control"
                    onChange={(event) => updateInputState(event, setEndDate)}
                />
                <label htmlFor="end_date">End Date</label>
                </div>
                <div className="form-floating mb-3">
                <h5>Description</h5>
                <textarea
                required name="description"
                id="description"
                className="form-control"
                onChange={(event) => updateInputState(event, setDescription)}></textarea>
                </div>
                <div className="form-floating mb-3">
                <input
                placeholder=""
                required type="number"
                name="max_presentations"
                id="max_presentations"
                className="form-control"
                onChange={(event) => updateInputState(event, setMaxPresentations)}
                />
                <label
                htmlFor="max_presentations">Max presentations
                </label>
                </div>
                <div className="form-floating mb-3">
                <input
                placeholder=""
                required type="number"
                name="max_attendees"
                id="max_attendees"
                className="form-control"
                onChange={(event) => updateInputState(event, setMaxAttendees)}
                />
                <label
                htmlFor="max_attendees">Max attendees
                </label>
                </div>
                <div className="mb-3">
                <select
                    required
                    name="location"
                    id="location"
                    className="form-select"
                    onChange={(event) =>
                    updateInputState(event, setSelectedLocation)
                    }
                >
                <option value="">Choose a location</option>
                {locations.map((location) => {
                return (
                    <option key={location.id} value={location.id}>
                    {location.name}
                    </option>
                    );
                })}
                </select>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
    </div>
    </>
);
}

export default ConferenceForm;
