import { NavLink } from "react-router-dom"

function Nav() {
return (
    <header>
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
        <a className="btn btn-primary me-2" href="http://localhost:3000/login" style={{backgroundColor: 'orange', borderColor: 'orange', color: 'white'}}>LOGIN</a>
        <a className="navbar-brand" href="http://localhost:3000/">Conference GO!</a>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <NavLink className="nav-link" aria-current="page" to="">Home</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" id="new-location" aria-current="page" to="/locations/new">New location</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" id="new-conference" aria-current="page" to="/conferences/new">New conference</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" id="new-presentation" aria-current="page" to="/presentations/new">New presentation</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" id="new-attendee" aria-current="page" to="/attendees/new">Attend Conference</NavLink>
                        </li>
                    </ul>
                </div>

        </div>
    </nav>
    </header>
);
}

export default Nav;
