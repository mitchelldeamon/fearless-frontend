import React, { useEffect, useState } from "react";

function LocationForm() {
    const [states, setStates] = useState([]);
    const [selectedState, setSelectedState] = useState("");
    const [name, setName] = useState("");
    const [room_count, setRoomCount] = useState("");
    const [city, setCity] = useState("");

    const updateInputState = function ({ target }, cb) {
        const { value } = target;

        cb(value);
    };

    const handleSubmit = async function (event) {
        event.preventDefault();
        const data = {
            state: selectedState,
            name,
            room_count,
            city,
        };

        const locationUrl = "http://localhost:8000/api/locations/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
            "Content-Type": "application/json",
            },
        };

        try {
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
            const newLocation = await response.json();
            console.log("Location created:", newLocation);
            } else {
            throw new Error("Network response was not ok");
            }
        } catch (error) {
            console.error("Error creating location:", error);
        }
        };

    const fetchData = async () => {
        const url = "http://localhost:8000/api/states/";

        try {
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setStates(data.states);
        } else {
            throw new Error("Network response was not ok");
        }
        } catch (error) {
        console.error("Error fetching data:", error);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <>
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a new location</h1>
                <form id="create-location-form" onSubmit={handleSubmit}>
                    <div className="form-floating mb-3">
                    <input
                        placeholder=""
                        required
                        type="text"
                        name="name"
                        id="name"
                        className="form-control"
                        onChange={(event) => updateInputState(event, setName)}
                    />
                    <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input
                        placeholder=""
                        required
                        type="number"
                        name="room_count"
                        id="room_count"
                        className="form-control"
                        onChange={(event) => updateInputState(event, setRoomCount)}
                    />
                    <label htmlFor="room_count">Room count</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input
                        placeholder=""
                        required
                        type="text"
                        name="city"
                        id="city"
                        className="form-control"
                        onChange={(event) => updateInputState(event, setCity)}
                    />
                    <label htmlFor="city">City</label>
                    </div>
                    <div className="mb-3">
                    <select
                        required
                        name="state"
                        id="state"
                        className="form-select"
                        onChange={(event) => updateInputState(event, setSelectedState)}
                    >
                        <option value="">Choose a state</option>
                        {states.map((state) => {
                        return (
                            <option key={state.abbreviation} value={state.abbreviation}>
                            {state.name}
                            </option>
                        );
                        })}
                    </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
            </div>
        </>
    );

}
export default LocationForm;
